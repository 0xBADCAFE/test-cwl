cwlVersion: v1.2

# What type of CWL process we have in this document.
class: Workflow

label: Q/A subworkflow

doc: Some documentation

inputs:
    answer:
        type: int
        label: Some answer
        doc: Some description of the answer
        format: TXT
    
    opt_arg:
        type: string?
        label: An optional argument
        doc: An optional argument that means nothing

outputs:
    answer_status:
        type: int
        label: Answer status
        doc: Status of the answer, 42 - success, 1 - failure
        outputSource: step-1/answer_status

steps:
    step-1:
        run: step-1.cwl
        in:
            answer: answer
            opt_arg: opt_arg
        out: [answer_status]


