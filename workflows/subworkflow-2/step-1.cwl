cwlVersion: v1.2

# What type of CWL process we have in this document.
class: CommandLineTool

label: Step One

doc: A docstring

baseCommand: [echo, -n]

inputs:
    answer: 
        type: int
        inputBinding:
            position: 1

outputs: 
    answer_status: 
        type: int