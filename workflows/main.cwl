cwlVersion: v1.2

# What type of CWL process we have in this document.
class: Workflow

label: Main Workflow

doc: A description of the Main Workflow, its goals and methods.

requirements:
    SubworkflowFeatureRequirement: {}

inputs:
    credentials:
        type: File
        label: Some credentials
        doc: Some login, password and other secrets
        format: TXT
    
    spell:
        type: string
        label: >
            Some very strong and very powerful MAGIC spell\n
            with a very long description which is attempted to be split\n
            in multiple lines
        doc: Some very strong and very powerful MAGIC spell with a very long description which is attempted to be split in multiple lines
        default: ABRACADABRA

    answer:
        type: int
        label: An answer
        doc: An answer to the Ultimate Question of Life, The Universe, and Everything
        default: 42

    opt_arg:
        type: string?
        label: An optional argument with a super long label\n that is attempted to be split in two lines
        doc: An optional argument that means nothing

outputs:
    question:
        type: string
        label: QUESTION
        doc: The Ultimate Question of Life, The Universe, and Everything
        outputSource: finish/question

steps:
    subworkflow-1:
        run: ./subworkflow-1/swf-1.cwl
        in:
            credentials: credentials
            spell: spell
        out: [spell_status] 

    subworkflow-2:
        run: ./subworkflow-2/swf-2.cwl
        in: 
            answer: answer
            opt_arg: opt_arg
        out: [answer_status]
    
    finish:
        run: finish.cwl
        in:
            s_status: subworkflow-1/spell_status
            a_status: subworkflow-2/answer_status
        out: [question]

    

  
  