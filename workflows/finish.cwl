cwlVersion: v1.2

# What type of CWL process we have in this document.
class: CommandLineTool

label: Last step

doc: A docstring

baseCommand: [echo, "What is the speed of an unladen swallow?"]

inputs:
    s_status: 
        type: int
        inputBinding:
            position: 1
    a_status:
        type: int
        inputBinding:
            position: 2

outputs: 
    question: string