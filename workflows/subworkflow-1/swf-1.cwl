cwlVersion: v1.2

# What type of CWL process we have in this document.
class: Workflow

label: Spelling subworkflow

doc: Login to spell server and perform a spell.

inputs:
    credentials:
        type: File
        label: Some credentials
        doc: Some login, password and other secrets
        format: TXT
    
    spell:
        type: string
        label: MAGIC spell
        doc: Some very strong and very powerful MAGIC spell\n with a very long description which is attempted to be split\n in multiple lines
        default: ABRACADABRA

outputs:
    spell_status:
        type: int
        label: Spell status
        doc: Status of the spelling, 0 is success, 1 is failure
        outputSource: step-2/status

steps:
    step-1:
        run: step-1.cwl
        in:
            credentials: credentials
        out: []

    step-2:
        run: step-2.cwl
        in:
            spell: spell
        out: [status]

