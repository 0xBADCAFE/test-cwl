cwlVersion: v1.2

# What type of CWL process we have in this document.
class: CommandLineTool

label: Step One

doc: A docstring

baseCommand: cat

inputs:
    credentials: 
        type: File
        inputBinding:
            position: 1

outputs: []