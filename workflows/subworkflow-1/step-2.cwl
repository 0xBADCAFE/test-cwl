cwlVersion: v1.2

# What type of CWL process we have in this document.
class: CommandLineTool

label: Step Two

doc: A docstring

baseCommand: echo 42

stdout: cwl.output.json

inputs:
    spell: 
        type: string

outputs: 
    status: int
